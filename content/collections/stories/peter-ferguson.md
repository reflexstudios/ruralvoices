---
id: 494e0a73-3899-46b1-9fdc-f1bd4b45146f
blueprint: stories
title: 'Peter Ferguson'
age: '76'
town: Garrison
county: fermanagh
images:
  - Image-of-Peter.jpg
  - Peter-with-Pam-and-grandkid.jpg
  - Sunser-in-Garrison.jpg
audio_highlights:
  -
    audio_highlight_title: 'Moving abroad and returning'
    audio_highlight_text: 'Peter talks about salmon fishing, the Troubles and his move abroad and return home.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Describing-Garrison-and-life-story.mp3
  -
    audio_highlight_title: 'Growing up in Garrison'
    audio_highlight_text: 'Peter shares memories of growing up in Garrison and reflects on the changes.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Changes-over-the-years.mp3
  -
    audio_highlight_title: 'Past traditions and local characters'
    audio_highlight_text: 'Peter recalls the céilí tradition, farming life and shares a story about local character Billy Ward.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Local-story-of-coile-and-radio.mp3
  -
    audio_highlight_title: 'Local tales'
    audio_highlight_text: 'Peter shares the story of ‘Old’ James and his alleged involvement in John Redmond’s Land League.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Local-story-Old-James-and-Irish-League.mp3
  -
    audio_highlight_title: 'Border stories'
    audio_highlight_text: 'Peter shares his memories of border living and his views on a United Ireland.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Border-and-United-Ireland.mp3
person_profile_image: Image-of-Peter.jpg
story_tags:
  - garrison
  - border
  - fermanagh
  - united-ireland
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646822414
main_audio: Peter-Ferguson-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Peter was born and raised in Garrison, Co. Fermanagh, but spent some time abroad before returning to his hometown in 1987. He talks about the salmon fishing tradition in Lough Melvin and shares local tales about céilí, radio listening and famous local characters. He highlights the beautiful surroundings and good community relations and discusses the current challenges of living in rural Fermanagh, including the lack of opportunity for young people, poor public transport and increasing costs of living. He also shares his memories of border living and his views on a United Ireland.'
---
