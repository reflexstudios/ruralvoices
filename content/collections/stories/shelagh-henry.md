---
id: 3e24f261-4497-4575-8264-90b24c6b7cb7
blueprint: stories
title: 'Shelagh Henry'
age: '62'
town: Rathfriland
county: down
images:
  - Shelagh.JPG
  - Shelagh-sunset.JPG
  - Shelagh-with-an-owl.JPG
audio_highlights:
  -
    audio_highlight_title: 'History of Rathfriland'
    audio_highlight_text: 'Shelagh describes Rathfriland and talks about its bustling market town past.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Describing-Rathfriland-and-changes.mp3
  -
    audio_highlight_title: 'Memories of the Troubles'
    audio_highlight_text: 'Shelagh recalls how the town was affected by the Troubles and shares some memories of it.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Impact-of-the-troubles.mp3
  -
    audio_highlight_title: 'Emigration stories'
    audio_highlight_text: 'Shelagh shares the stories of two local émigrés: Patrick Brontë, father of the Brontë sisters, and Catherine Schubert O’Hare, one of the pioneers of the Canadian gold rush.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Bronte-and-Catherine-Ohare.mp3
  -
    audio_highlight_title: 'Graham''s Ice Cream'
    audio_highlight_text: 'Shelagh shares the story behind the famous Graham''s Ice Cream.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Grahams-Ice-cream.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'Shelagh talks about her hopes for a future Rathfriland rid of flags and with a bustling trade scene.'
    type: new_audio
    enabled: true
    audio_highlight_file: '5.-Dereliction,-flags-and-future-hopes.mp3'
person_profile_image: Shelagh.JPG
story_tags:
  - women
  - migration
  - rathfriland
  - heritage
  - troubles
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646822340
main_audio: Shelagh-Henry-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Shelagh talks about Rathfriland’s past as a bustling market town and shares memories of growing up there and being surrounded by nature. She recalls how the town was affected by the Troubles and highlights the importance of local community groups. She also talks about her area’s connection with emigration and shares the stories of two famous émigrés: Patrick Brontë, father of the Brontë sisters, and Catherine Schubert O’Hare, one of the pioneers of the Canadian gold rush. She also shares the story behind the famous Graham Icecream and her hopes for a future with no flags and a bustling trade scene.'
---
