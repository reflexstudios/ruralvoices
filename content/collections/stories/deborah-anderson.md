---
id: c0a40ec8-bd63-4263-9e96-1a40b6b320e7
blueprint: stories
title: 'Deborah Anderson'
age: '58'
town: Glenavy
county: antrim
images:
  - Deborah-on-farm.JPG
  - Deborah-portrait.jpg
audio_highlights:
  -
    audio_highlight_title: 'Rural childhood'
    audio_highlight_text: 'She describes her rural area and moving there to give her children a rural childhood.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-to-her-area.mp3
  -
    audio_highlight_title: 'My favourite things'
    audio_highlight_text: 'She shares her favourite things about living in her rural area: the views, quietness and space.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-favourite-things.mp3
  -
    audio_highlight_title: 'Community dynamics'
    audio_highlight_text: 'She talks about the sense of belonging to the area and community dynamics.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Sense-of-community.mp3
  -
    audio_highlight_title: 'Past and future changes'
    audio_highlight_text: 'She reflects on the past and future of her rural community.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Past-and-future-changes.mp3
person_profile_image: Deborah-portrait.jpg
story_tags:
  - glenavy
  - infrastructure
  - nature
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821068
main_audio: Deborah-Anderson-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Deborah talks about moving to a 100-year-old house located between Gleneavy and Lisburn to give her children a rural childhood. She shares her love for the views, the quietness and the space and talks about how community life revolves around the church, school and local shops. She also highlights the lack of frequent public transport services and broadband issues in the area and reflects on the past and future of her rural community.'
---
