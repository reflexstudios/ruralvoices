---
id: 17fdcf01-90a0-4f9f-a91d-933552cbe309
blueprint: stories
title: 'Rosie Finnegan-Bell'
age: '51'
town: Cullaville
county: armagh
images:
  - people/rosie-photo.jpg
  - people/five-generations-of-lacemakers-cropped.png
  - people/Another-generation-joins-the-family-trade.jpg
audio_highlights:
  -
    audio_highlight_title: 'From hobby to business'
    audio_highlight_text: 'Rosie talks about how lace-making went from a hobby to a sustainable enterprise in South Armagh.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1-Rosie-talks-about-the-start-of-lace-tradition.mp3
  -
    audio_highlight_title: 'The South Armagh Lace Collective'
    audio_highlight_text: 'Rosie talks about setting up the South Armagh Lace Collective to promote this rich tradition.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2-Rosie-talks-about-SA-Lace-Collective.mp3
  -
    audio_highlight_title: 'Describing South Armagh'
    audio_highlight_text: 'Rosie describes the natural beauties of South Armagh, including the mighty Slieve Gullion and Lough Ross.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3-She-talks-about-the-natural-beauties.mp3
  -
    audio_highlight_title: 'Changing the narrative'
    audio_highlight_text: 'Rosie talks about the negative labels that the area has received and how local people feel about them.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4-Negative-labels-and-pride-of-the-place.mp3
  -
    audio_highlight_title: 'Impact of partition'
    audio_highlight_text: 'Rosie reflects on the impact of partition and the border on lace-making, trading and farming.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5-She-talks-the-impact-of-partition.mp3
person_profile_image: people/rosie-photo.jpg
story_tags:
  - cullaville
  - south-armagh
  - lace-making
  - border
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646819020
main_audio: Rosie-Bell-Full-Interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Rosie Finnegan-Bell shares her pride in South Armagh. She talks about the long-standing lace-making tradition in the area and the South Armagh Lace Collective which she set up with a group of women in 2017. Rosie also describes the natural beauties of South Armagh, including the mighty Slieve Gullion and Lough Ross, and talks about the impact of the Troubles in the area. She also discusses how lace-making, trading and farming have been affected by partition over the past 100 years and why she would not like to see a hard border again.'
---
