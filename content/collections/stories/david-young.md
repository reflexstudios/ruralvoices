---
id: 73addcfa-50c7-47f1-83bb-f81deba8ba2e
blueprint: stories
title: 'David Young'
age: '64'
town: Castlederg
county: tyrone
images:
  - David.jpg
  - Castlederg.jpg
  - Mens-shed.jpg
audio_highlights:
  -
    audio_highlight_title: 'Rural living'
    audio_highlight_text: 'David talks about Castlederg and how the ‘rural’ label has changed over the years.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Rural-label.mp3
  -
    audio_highlight_title: 'Community spirit'
    audio_highlight_text: 'David highlights the town’s community spirit, particularly in times of Covid.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Community-Spirit.mp3
  -
    audio_highlight_title: Sectarianism
    audio_highlight_text: 'David discusses the issue of sectarianism in his rural community.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Troubles-and-sectarianism.mp3
  -
    audio_highlight_title: 'Dental Services in rural communities'
    audio_highlight_text: 'David reflects on how dentistry has changed over the years.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Dentistry.mp3
  -
    audio_highlight_title: 'Going viral on TikTok'
    audio_highlight_text: 'David shares his love for community work and a funny story about going viral on TikTok.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Community-work-and-tiktok.mp3
person_profile_image: David.jpg
story_tags:
  - castlederg
  - dentistry
  - community-relations
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821913
main_audio: David-Young-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: "David talks about his pride in Castlederg and reflects on how the ‘rural’ label has changed over the years. He talks about the community spirit in his community but also how sectarianism remains the main challenge. He describes his work as a dentist and how dentistry has changed over the years. He also shares his love for community work, recalls a funny story about going viral on TikTok and talks about his hopes for the future.\_"
---
