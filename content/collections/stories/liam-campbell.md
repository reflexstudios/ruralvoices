---
id: 394a0c2f-1267-4a0b-9c8b-5c5a22f7bf0e
blueprint: stories
title: 'Liam Campbell'
age: '59'
town: Plumbridge
county: tyrone
images:
  - people/liam-rainbow.jpg
  - people/liam-wife-daughter.jpg
  - people/liam-daughter.jpg
audio_highlights:
  -
    audio_highlight_title: 'Sperrins and boglands'
    audio_highlight_text: 'Liam reflects on his identity and shares his love for the Sperrins and its boglands.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1-Shares-passion-for-the-boglands.mp3
  -
    audio_highlight_title: 'Economic activity in the Sperrins'
    audio_highlight_text: 'Liam talks about how the Sperrins have changed from being regarded as unproductive to resource-rich.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2-Bogland-resources-have-changed.mp3
  -
    audio_highlight_title: 'A Poitín-making story'
    audio_highlight_text: 'Liam shares a funny story about Poitín-making and dead sheep in the Sperrins.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3-Poitin-Story.mp3
  -
    audio_highlight_title: 'Depopulation in the Sperrins'
    audio_highlight_text: 'Liam talks about depopulation and how working from home may help bring people back to the Sperrins.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4-Populating-the-Sperrins-again.mp3
  -
    audio_highlight_title: 'Land and ownership'
    audio_highlight_text: 'Liam talks about how land and ownership is an untold story that has impacted people’s mental health.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5-Land-Ownership-and-Mental-Health.mp3
person_profile_image: people/liam-rainbow.jpg
story_tags:
  - sperrins
  - boglands
  - poitin
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646819094
main_audio: Liam-Campbell-Full-Interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Liam Campbell was born in rural Donegal but has lived in the Sperrins for most of his adult working life. He talks about the place’s dynamic history despite its hilly and bogland nature and how it has gone from being regarded as unproductive for farming to resource-rich for gold mining and renewable energy. He also shares his love for the Sperrins and an anecdote about its rich Poitín-making tradition. He also draws attention to the issue of land and ownership and their connection to mental health and shares his hopes for the future.'
---
