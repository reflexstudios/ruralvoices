---
id: 0860dff2-6d2d-4388-b77c-b559dea13244
blueprint: stories
title: 'Jessica (Thi Poh Kian) & Kevin Chaffey'
age: '62 & 77'
town: Enniskillen
county: fermanagh
images:
  - people/Jessica-and-Kevin.jpg
  - people/Family-pic.JPG
  - people/Jessica-and-Kevin2.JPG
audio_highlights:
  -
    audio_highlight_title: 'First impressions of Enniskillen'
    audio_highlight_text: 'They talk about moving to Northern Ireland and share their first impressions of Enniskillen.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-to-NI-and-first-impressions-1646819354.mp3
  -
    audio_highlight_title: 'Reflections on identity and belonging'
    audio_highlight_text: 'They reflect on their outsider identity and experiences of discrimination.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Being-outsiders-and-discrimination--1646819355.mp3
  -
    audio_highlight_title: 'Our favourite things'
    audio_highlight_text: 'They talk about their favourite things about living in a rural town, including going litter-picking.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Favourite-parts-and-litter-picking.mp3
  -
    audio_highlight_title: 'Adapting to county Fermanagh'
    audio_highlight_text: 'They recall adapting to county Fermanagh, the ‘rural accent’ and to the contested nature of flags and language.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Getting-used-to-flags-accents-politics-BEST.mp3
person_profile_image: people/Jessica-and-Kevin.jpg
story_tags:
  - bame
  - migration
  - fermanagh
  - enniskillen
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646819509
main_audio: Jessica-and-Kevin-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Originally from Malaysia and England, Jessica and Kevin, respectively, talk about moving to Northern Ireland in 2009 and having to adapt to life in a rural town as a retired couple. They reflect on their ‘outsider’ identity and its impact on their daily lives. They also share what they love most about county Fermanagh - the people and the nature - and also highlight some of the limitations of living in a rural city. Jessica and Kevin share their hopes for younger and older generations in county Fermanagh.'
---
