---
id: 862d2e5e-b7a7-453a-a074-6665d4639f10
blueprint: stories
title: 'Lorna Dunn'
age: '53'
town: Killylea
county: down
images:
  - 'people/2. Lorna Slieve Croob.jpg'
  - 'people/1. Lorna and Husband in the Mournes.jpg'
  - 'people/3. Coffee time.jpg'
main_audio: Lorna-Dunn-Full-Interview.mp3
audio_highlights:
  -
    audio_highlight_title: 'First impressions'
    audio_highlight_text: 'Lorna shares her first impressions of Northern Ireland after moving from Edinburgh to Killylea.'
    audio_highlight_file: 1.-Lorna---First-impressions-of-NI.mp3
    type: new_audio
    enabled: true
  -
    audio_highlight_title: 'The 12th of July and murals'
    audio_highlight_text: 'Lorna recalls her reactions to the 12th of July Marching Season and paramilitary murals.'
    audio_highlight_file: 2.-Lorna---division-and-paramilitaries-BEST.mp3
    type: new_audio
    enabled: true
  -
    audio_highlight_title: 'Adapting to farming life'
    audio_highlight_text: 'Lorna describes her unconventional farmer’s wife role and adapting to farming and rural life.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Lorna---Adapting-to-farming-life.mp3
  -
    audio_highlight_title: 'Describing Killylea'
    audio_highlight_text: 'Lorna shares what she likes most about Killylea and County Down.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Lorna---What-i-love-about-here.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'Lorna shares her hopes for a less segregated future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Lorna---Hopes-for-the-future.mp3
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646818733
person_profile_image: 'people/1. Lorna and Husband in the Mournes.jpg'
story_tags:
  - farming
  - education
  - flags-and-murals
  - killylea
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Lorna Dunn moved from Edinburgh (Scotland) to Killylea (Co. Down) when she was 26, after meeting and marrying her farming husband. Lorna talks about her adaption to farming life and Northern Irish politics, threats received from paramilitaries, her contribution to the local community, and what she likes most about her rural area.'
---
