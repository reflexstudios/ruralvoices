---
id: d08d491c-0858-41c9-a18f-23e76bd61125
blueprint: stories
title: 'Matthew Beaumont'
age: '34'
town: Omagh
county: tyrone
images:
  - people/Matt-and-son-2.jpeg
  - people/Matt-and-son.jpeg
  - people/Matt-and-wife.jpeg
audio_highlights:
  -
    audio_highlight_title: 'First impressions of Northern Ireland'
    audio_highlight_text: 'Matthew talks about his first impressions of Northern Ireland after moving from London.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1-Moving-to-NI-and-first-impressions.mp3
  -
    audio_highlight_title: 'Being British in county Tyrone'
    audio_highlight_text: 'Matthew recalls some situations because of his British identity and how he learned to deal with them.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2-Experiences-as-an-Englishman.mp3
  -
    audio_highlight_title: 'The challenges of rural living'
    audio_highlight_text: 'Matthew talks about the challenges of rural living, including equal access to broadband, jobs, health service, and schools.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3-Main-challenges-today.mp3
  -
    audio_highlight_title: 'A bright future ahead'
    audio_highlight_text: 'Matthew shares his optimism for the future of Northern Ireland.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4-Hopes-for-the-future.mp3
person_profile_image: people/Matt-and-son.jpeg
story_tags:
  - migration
  - peace-building
  - politics
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646819166
main_audio: Matt-Beaumont-Full-Interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Matthew was born in London but moved to Omagh, Co. Tyrone, nine years ago after meeting his veterinarian wife. He talks about his first impressions of Northern Ireland and about his work with the Alliance Party and community organisations. Matthew also lists a few challenges of rural living, including equal access to broadband, jobs, health service, and schools and shares his optimism for the future of his local community.'
---
