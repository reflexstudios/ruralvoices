---
id: 7c477424-39f5-49dc-9e68-b9be6d1b784c
blueprint: stories
title: 'Margaret Masaba'
age: '39'
town: 'Ligford Road'
county: tyrone
images:
  - Margaret-Masaba.JPG
  - Snowy-road.JPG
  - Turf-cutting.jpg
audio_highlights:
  -
    audio_highlight_title: 'From Kenya to Tyrone'
    audio_highlight_text: 'Margaret talks about moving from Kenya and her first impressions of rural Northern Ireland.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-to-NI.mp3
  -
    audio_highlight_title: 'Defining her local area'
    audio_highlight_text: 'Margaret describes how the area is defined by the terrain, forest and fresh air.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Defining-her-local-area.mp3
  -
    audio_highlight_title: 'Turf Tradition'
    audio_highlight_text: 'Margaret shares her passion for the turf tradition and highlights its role in community relations.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Turf-Story.mp3
  -
    audio_highlight_title: 'Challenges of rural living'
    audio_highlight_text: 'Margaret discusses the challenges of rural living, including isolation and poor infrastructure.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Main-challenges.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'Margaret talks about her mixed marriage and shares hopes for a greener and more diverse future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Future-hopes.mp3
person_profile_image: Margaret-Masaba.JPG
story_tags:
  - bame
  - migration
  - heritage
  - tyrone
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821981
main_audio: Margaret-Masaba-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Margaret moved from Kenya to county Tyrone to be with her Irish partner three years ago. She talks about how she settled in and describes her local area as surrounded by nature and fresh air. She shares her love for the countryside and the Irish tradition of turf cutting which she believes is a great community activity. She discusses the challenges of rural living, particularly isolation, and shares her hopes for a greener and more diverse future.'
---
