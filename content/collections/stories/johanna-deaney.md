---
id: eacb1097-80c7-4e39-a84f-baeb2713906e
blueprint: stories
title: 'Johanna Deaney'
age: '62'
town: Ballyclare
county: antrim
images:
  - Johanna-beach.jpg
  - Johanna-nature.jpeg
  - Johanna-running.jpeg
audio_highlights:
  -
    audio_highlight_title: 'Moving to Ballyclare'
    audio_highlight_text: 'Johanna talks about how a chip shop made her move from North Belfast to Ballyclare.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-to-Ballyclare-and-describing-it.mp3
  -
    audio_highlight_title: 'Defining Ballyclare'
    audio_highlight_text: 'Johanna describes her favourite thing about Ballyclare and what best defines it.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Favourite-thing-and-defines-community.mp3
  -
    audio_highlight_title: 'Local History'
    audio_highlight_text: 'Johanna describes how this rural town went from being a mill-farming town to a commuter town.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Changes.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'Johanna shares her hopes for Ballyclare’s future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Hopes-for-the-future.mp3
person_profile_image: Johanna-beach.jpg
story_tags:
  - ballyclare
  - farming
  - mills
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821369
main_audio: Johanna-Deaney-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Johanna was born and grew up in North Belfast and was ‘convinced’ to move to Ballyclare by one of the local chip shops. She describes how much this rural town has changed from being a mill-farming town to a commuter town. She talks about the town’s sense of community and how she has integrated by taking part in a number of activities including church-going, park run, and so forth. She believes that transport remains the main challenge and shares her hopes for a bustling future for its high street.'
---
