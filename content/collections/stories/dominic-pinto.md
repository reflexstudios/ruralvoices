---
id: f76bf0a7-16db-46ef-852e-db1b06a16609
blueprint: stories
title: 'Dominic Pinto'
age: '84'
town: Omagh
county: tyrone
images:
  - people/dr-pinto.jpeg
audio_highlights:
  -
    audio_highlight_title: 'From India to Omagh'
    audio_highlight_text: 'Dominic shares his journey from India to Omagh and how his family was affected by the Troubles.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1-Moving-to-NI-and-Troubles.mp3
  -
    audio_highlight_title: 'Catholic/Protestant segregation'
    audio_highlight_text: 'Dominic recalls his reactions to the Catholic/Protestant segregation and how he deals with it.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2-First-reactions-to-the-segregation-BEST.mp3
  -
    audio_highlight_title: 'Medical services in Tyrone'
    audio_highlight_text: 'Dominic talks about his role in improving medical services for rural people in county Tyrone.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3-Improving-medical-services-in-the-West.mp3
  -
    audio_highlight_title: 'My favourite thing'
    audio_highlight_text: 'Dominic shares his favourite thing about Northern Ireland: the golf courses and the people.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4-Love-for-the-place-and-community.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'Dominic talks about the importance of looking after the elderly population and integrated education.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5-Hopes-for-the-future-1646818796.mp3
person_profile_image: people/dr-pinto.jpeg
story_tags:
  - medicine
  - migration
  - omagh
  - tyrone
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646818835
main_audio: Dominic-Pinto-Full-Interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Dr Pinto shares his journey from India to Omagh and how he has contributed to improving medical services in county Tyrone. He recalls his family’s experiences of the Troubles and how he dealt with the Catholic/Protestant segregation at work, family and social life. He also talks about his work with migrant and ethnic minority groups and shares his favourite spots and things about Northern Ireland: the golf courses and the people. He shares his hopes for the future across young and old generations.'
---
