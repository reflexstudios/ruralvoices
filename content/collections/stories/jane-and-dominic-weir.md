---
id: 9386c557-8993-4e39-b055-0a11b8b17cf4
blueprint: stories
title: 'Jane & Dominic Weir'
age: '55 & 60'
town: Garrison
county: fermanagh
images:
  - dom-and-jane.JPG
  - lough-melvin.jpg
audio_highlights:
  -
    audio_highlight_title: 'Moving to Garrison from England'
    audio_highlight_text: 'They talk about moving to Garrison from England and first impressions.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-to-Garrison.mp3
  -
    audio_highlight_title: 'Rural living for teenagers'
    audio_highlight_text: 'They highlight the lack of activities for teenagers and reflect on Northern Ireland’s school system.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Nothing-for-teenagers.mp3
  -
    audio_highlight_title: 'Integrated education'
    audio_highlight_text: 'They share their views on integrated schooling and their daughter’s experience of education.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-NI-Education-System.mp3
  -
    audio_highlight_title: 'Moving on from the Troubles'
    audio_highlight_text: 'They talk about the misconceptions about Fermanagh and how the county has moved on from the Troubles.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Views-on-the-troubles.mp3
  -
    audio_highlight_title: 'Views on a United Ireland'
    audio_highlight_text: 'They share their views on a United Ireland and hopes for the future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-United-Ireland-1646822963.mp3
person_profile_image: dom-and-jane.JPG
story_tags:
  - education
  - border
  - troubles
  - migration
  - garrison
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646822972
main_audio: Jane-and-Dominic-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Jane and Dominic moved from England to Garrison in search of a scenic retired life. They talk about the surrounding nature, community relations, lack of activities for teenagers and share their experience of the rural education system. Jane and Dominic also talk about the misconceptions about Fermanagh while growing up in a Protestant household in Tyrone and England, respectively, and how the place has moved on from the Troubles. They also share their views on a United Ireland and their hopes for the future of their local area.'
---
