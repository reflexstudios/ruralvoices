---
id: 2e040b6b-4c7a-4b91-9b27-8f89d0eda704
blueprint: stories
title: 'Robert Carmichael'
age: '63'
town: Dungiven
county: derry
images:
  - Robert-1.jpeg
  - Robert-family.jpeg
audio_highlights:
  -
    audio_highlight_title: 'Farming changes'
    audio_highlight_text: 'Robert reflects on how farming changed from being a diverse family tradition to large-scale production.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Changes-in-Farming-1646822769.mp3
  -
    audio_highlight_title: 'Views on Brexit'
    audio_highlight_text: 'Robert discusses the impact of the European Union on farmers and shares his views on Brexit.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-EU-Brexit-and-Protocol-1646822781.mp3
  -
    audio_highlight_title: 'Farming and climate change'
    audio_highlight_text: 'Robert talks about the parliamentary discussions about making farming more environmentally friendly.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Farming-and-Clima-change.mp3
  -
    audio_highlight_title: 'Community relations'
    audio_highlight_text: 'Robert talks about the good relations between farmers from all backgrounds.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Famers-relations.mp3
  -
    audio_highlight_title: 'United Ireland'
    audio_highlight_text: 'Robert shares his views on the United Ireland debate.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-United-Ireland.mp3
person_profile_image: Robert-1.jpeg
story_tags:
  - farming
  - climate-change
  - community-relations
  - united-ireland
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646822888
main_audio: Robert-Carmichael-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: "Robert talks about the main challenges for farmers in Northern Ireland today.\_ He reflects on how mechanisation and the joining of the European Single Market changed farming activity from a diverse family tradition to large-scale production. He also shares his views on Brexit, the protocol and a United Ireland and discusses the environmental issues faced by farmers. He talks about his local area, the scenic Roe Valley, community relations between Catholic and Protestant farmers and hopes for the future."
---
