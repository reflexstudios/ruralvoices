---
id: 721a01e6-3329-46d5-87d0-cbdbc4b24979
blueprint: stories
title: 'Bronagh McAtasney'
age: '54'
town: Mayobridge
county: down
images:
  - Bronagh.jpg
  - Civil-rights.jpg
  - Snowy-day.jpg
audio_highlights:
  -
    audio_highlight_title: 'Describing Mayobridge'
    audio_highlight_text: 'She talks about moving to Maybridge and describes its beautiful surroundings.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-to-Mayobridge-and-scenic-views.mp3
  -
    audio_highlight_title: 'A pandemic story'
    audio_highlight_text: 'She recalls the day she went for a Covid injection and ‘came back’ with a relative.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Covid-story.mp3
  -
    audio_highlight_title: 'The unfinished Civil Rights March'
    audio_highlight_text: 'She talks about local history, including the unfinished Civil Rights March in Newry.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Civil-Rights-March.mp3
  -
    audio_highlight_title: 'My favourite thing'
    audio_highlight_text: 'She shares her favourite thing about her rural area: the strength of local women.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Favourite-thing---women.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'She discusses current issues affecting her local area and shares her hopes for the future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Current-Issue-and-Future-hopes.mp3
person_profile_image: Bronagh.jpg
story_tags:
  - newry
  - women
  - civil-rights
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821255
main_audio: Bronagh-McAtasney-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Bronagh moved from Holywood, North Down, to Mayobridge, South Down, in the late 1970s and talks about her love and pride for her adopted home. She highlights the beautiful surroundings, such as the Mourne Mountains and Carlingford Lough, its rich GAA history and folk tradition. She also shares anecdotes about rural life and recalls the unfinished Civil Rights march and how she was able to bring this story back to life 50 years later. She talks about local women’s strengths and about the challenges her area faces and her hopes for the future.'
---
