---
id: e601b378-f126-455e-ac03-c1c37fabd6ca
blueprint: stories
title: 'Mary & Michael Wailes'
age: '71 & 79'
town: Cullaville
county: armagh
images:
  - people/unnamed-1646398648.jpg
  - people/IMG_0455.jpg
  - people/unnamed-4.jpg
audio_highlights:
  -
    audio_highlight_title: 'Petrol and Pints'
    audio_highlight_text: 'They talk about moving to Cullaville and finding a thriving community where it’s easier to find petrol than pints.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Describing-Cullaville.mp3
  -
    audio_highlight_title: 'Challenges of rural living'
    audio_highlight_text: 'They discuss the challenges of living in a rural village including reduced library services, lack of public transport and having an ‘outsider’ status.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Moving-to-a-rural-area.mp3
  -
    audio_highlight_title: 'Lace-making in South Armagh'
    audio_highlight_text: 'Mary talks about the lace-making tradition in South Armagh, how she got involved and her hopes for its future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Lace-Making-Tradition.mp3
  -
    audio_highlight_title: 'Past and future changes'
    audio_highlight_text: 'They talk about how Cullaville has changed over the years and how the area has overcome the ‘Bandit Country’ label.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Bandit-Country-reputation-and-changes.mp3
person_profile_image: people/unnamed.jpg
story_tags:
  - lace-making
  - south-armagh
  - cullaville
  - border
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646818915
main_audio: Michael-and-mary-full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Mary and Michael Wailes moved from North Yorkshire to Cullaville, South Armagh, to be closer to their daughter and to reconnect with their family roots. They talk about the history of Cullaville and how business, sport, and community have changed in this thriving part of South Armagh. They describe their experience of lockdown during the pandemic and their involvement in a number of social activities, including lace-making. They talk about the challenges of rural living, the isolation and lack of infrastructure as well as how South Armagh has moved away from the ‘Bandit Country’ label.'
---
