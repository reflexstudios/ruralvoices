---
id: fc53920c-9e29-4557-bbfc-2fade20dd2e0
blueprint: stories
title: 'Aidan Campbell'
age: '49'
town: Derrytresk
county: tyrone
images:
  - Aidan3.jpg
  - 'Derrytresk-Johnson''s-Rampart.jpg'
  - Derrytresk-track-from-Coalisland-Canal.jpg
audio_highlights:
  -
    audio_highlight_title: 'Farming in Derrytresk'
    audio_highlight_text: 'Aidan describes Derrytresk and how farming in the area has changed over the years.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Describing-the-area.mp3
  -
    audio_highlight_title: 'Local community'
    audio_highlight_text: 'Aidan shares his love for the boglands and the strong community spirit.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Favourite-things.mp3
  -
    audio_highlight_title: 'The Troubles in East Tyrone'
    audio_highlight_text: 'Aidan discusses the impact of the Troubles on East Tyrone.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Impact-of-the-Troubles.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'Aidan talks about the current challenges and his hopes for a greener future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Main-challenges-and-hopes-for-the-future.mp3
person_profile_image: Aidan3.jpg
story_tags:
  - east-tyrone
  - troubles
  - farming
  - boglands
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821310
main_audio: Aidan-Campbell-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Aidan talks about growing up on a farm and how farming in his area has changed over the years. He also discusses how East Tyrone has been badly affected by the Troubles and its impact on society. He also shares his love for nature and the boglands as well as for the strong community spirit in the area. He talks about the challenges his area faces in relation to education, health service, infrastructure and climate change. He shares his hopes for a future where young people do not feel that they have to emigrate and farming is more in tune with nature.'
---
