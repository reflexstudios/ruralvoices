---
id: 405fe387-b243-4913-b2c3-22ce78d39a38
blueprint: stories
title: 'Frank McHugh'
age: '58'
town: 'Monea & Boho'
county: fermanagh
images:
  - Frank-and-wife.jpg
  - Portrait-of-Frank.jpeg
audio_highlights:
  -
    audio_highlight_title: 'Leaving and returning to Fermanagh'
    audio_highlight_text: 'Frank talks about growing up and moving back to Fermanagh.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-back-to-Fermanagh.mp3
  -
    audio_highlight_title: 'Describing Monea and Boho'
    audio_highlight_text: 'Frank defines his rural area by its geography, nature, history and good community relations.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Describing-the-area.mp3
  -
    audio_highlight_title: 'Some childhood memories'
    audio_highlight_text: 'Frank shares childhood memories of Belmore Mountain and talks about how Boho is connected to the famous TV show Game of Thrones.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Favourite-thing-and-GOT.mp3
  -
    audio_highlight_title: 'County Fermanagh today'
    audio_highlight_text: 'Frank talks about how his area has moved away from the Troubles, the lack of opportunities for young people and the importance of travelling.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Lack-of-opportunities-for-kids.mp3
  -
    audio_highlight_title: 'Touristic potential'
    audio_highlight_text: 'Frank discusses how tourism has changed in county Fermanagh and the impact on rural communities.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5-Tourism-Industry-challenges.mp3
person_profile_image: Portrait-of-Frank.jpeg
story_tags:
  - monea
  - boho
  - migration
  - tourism
  - heritage
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821741
main_audio: Frank-McHugh-Full-Interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Frank talks about growing up in the parish of Boho, Derrygonelly and Monea, Co. Fermanagh, and moving around the country. He left for England when he was 18 and moved back to the area with his family in 2006. He describes how the area is defined by its geography and nature, rich history, and community relations as well as how much it has changed over the years. Frank discusses the role of tourism in the area and shares his hopes for more opportunities for young people, remote working, affordable housing and sustainable tourism.'
---
