---
id: 99e4ff9c-f2c6-4370-9c10-2e8be5928604
blueprint: stories
title: 'Molly Farrell'
age: '22'
town: Dungannon
county: tyrone
story_tags:
  - lgbtq
  - pride
  - youth
  - mid-ulster
  - dungannon
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646824213
images:
  - people/168558644_4388412207854231_6552082344991854801_n.jpg
  - people/IMG20210518123413.jpg
  - people/IMG20210616113350.jpg
audio_highlights:
  -
    audio_highlight_title: 'The vet neighbour'
    audio_highlight_file: 1-Molly-and-the-vet-practice.mp3
    type: new_audio
    enabled: true
    audio_highlight_text: 'Molly talks about the neighbouring veterinary practice and how it made a big impression on her.'
  -
    audio_highlight_title: 'Local history'
    audio_highlight_file: 2-History-of-Dungannon-1646504959.mp3
    type: new_audio
    enabled: true
    audio_highlight_text: 'Molly shares the history associated with Dungannon, including the linen factories, bombings, and the Red Hand of Ulster.'
  -
    audio_highlight_title: 'Falling in love with Dungannon'
    audio_highlight_text: 'Molly describes how she feels about Dungannon and falling in love with the Hill of the O’Neill during the lockdown.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3-What-She-likes-the-Most.mp3
  -
    audio_highlight_title: 'Mid-Ulster Pride'
    audio_highlight_text: 'Molly talks about how she helped set up Mid Ulster Pride and the reactions to their first parade.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4-Setting-up-Mid-Ulster-Pride.mp3
  -
    audio_highlight_title: 'LGBTQ+ and Protestantism'
    audio_highlight_text: 'Molly reflects on her LGBTQ+ activism and her Protestant faith.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Faith-and-Activism.mp3
  -
    audio_highlight_title: 'Challenges of rural living'
    audio_highlight_text: 'Molly highlights the main challenge of rural living: the lack of food delivery services.'
    audio_highlight_file: 6-Delivery-and-Internet-1646505039.mp3
    type: new_audio
    enabled: true
person_profile_image: people/IMG20210518123413.jpg
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
main_audio: Molly-Farrell-Full-Interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Molly Farrell was born and grew up outside of Dungannon and has lived mostly there. She talks about the pros and cons of being a ‘country bumpkin’ and her advocacy work with Mid-Ulster Pride. She also highlights the historical importance of her rural area and how it has been shaped by linen factories, the Troubles, industrialisation and nature. She shares her hopes for Mid-Ulster with more open and green spaces, more progression with LGBTQ+ rights and where the rural way of living is maintained.'
---
