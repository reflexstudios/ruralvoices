---
id: 1f16de5a-240c-4923-911c-b5a74869c3f6
blueprint: stories
title: 'Barry Murray'
age: '66'
town: Eshnascreen
county: fermanagh
images:
  - Mullynaburtlan-Rd.jpg
audio_highlights:
  -
    audio_highlight_title: 'Shopkeeping work'
    audio_highlight_text: 'Barry talks about his experience of shopkeeping and how it has changed over the years.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Shopkeeping.mp3
  -
    audio_highlight_title: 'Eshnascreen over the years'
    audio_highlight_text: 'Barry describes how Eshnascreen has changed over the years and the impact of depopulation/population.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Changes.mp3
  -
    audio_highlight_title: 'Community relations'
    audio_highlight_text: 'Barry highlights the importance of schools, shops, post offices, churches and sports clubs for community cohesion.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Community-Cohesion.mp3
  -
    audio_highlight_title: 'Living in Fermanagh'
    audio_highlight_text: 'Barry talks about what defines Fermanagh and what he likes and dislikes about it.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Definign-the-local-area.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'Barry shares his thoughts on working from home, current challenges and hopes for the future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Hopes-for-the-future-1646823144.mp3
person_profile_image: Mullynaburtlan-Rd.jpg
story_tags:
  - community-relations
  - business
  - fermanagh
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646823152
main_audio: Barry-Murray-Full-1646823094.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Barry describes how rural life has changed in Eshnascreen which experienced a period of depopulation in the 1970s and 1980s and for the last few years has gone through a period of high population growth. He talks about the impact of these changes on local shops, schools and community cohesion and shares his favourite things about this part of Fermanagh and some of the challenges of living there. He also reflects on the current dynamics brought by the pandemic, particularly working from home and how this could potentially be used to make living in towns like his more sustainable and prosperous.'
---
