---
id: 0718f139-eab3-4456-bf70-6babaca73282
blueprint: stories
title: 'Roberta Bacic & Clem McCartney'
age: '72 & 75'
town: Benone
county: derry
images:
  - Clem-and-Roberta.jpg
  - benonedunes.JPG
audio_highlights:
  -
    audio_highlight_title: 'Peace-building work'
    audio_highlight_text: 'They talk about moving to Benone and their peace-building and storytelling work.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-to-Benone-and-jobs.mp3
  -
    audio_highlight_title: 'The triangle of land'
    audio_highlight_text: 'They describe Benone as a triangle of land between Lough Foyle, the Atlantic and the mountain and the community dynamics.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Describing-and-adapting-to-Benone.mp3
  -
    audio_highlight_title: 'What we like the most'
    audio_highlight_text: 'They describe what they like most about their rural area: the nature and the absence of sectarianism.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Nature-and-sectarianism.mp3
  -
    audio_highlight_title: 'Jam-making and integration'
    audio_highlight_text: 'Roberta talks about using jam-making as a way of integrating and shares an anecdote about her South American identity.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Jam-and-Chilean-anecdotes.mp3
  -
    audio_highlight_title: 'Good community relations'
    audio_highlight_text: 'They describe rural and urban community dynamics and share their thoughts on good community relations.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Community-Dynamics.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'They share the economic, environmental and societal challenges and their hopes for the future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 6.-Challenges-and-hopes-future.mp3
person_profile_image: Clem-and-Roberta.jpg
story_tags:
  - migration
  - peace-building
  - benone
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646820349
main_audio: Clem-and-Roberta-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Antrim-born Clem moved to Benone, Co. Derry, in the late 1970s and was joined by his Chilean wife Roberta a few years later. They share their love for this triangle of land between Lough Foyle, the Atlantic and the mountain and talk about integration. Clem and Roberta also describe the community dynamics and how these have changed in times of Covid. They discuss sectarianism in urban and rural areas and recall experiences as ‘outsiders’, including making jam as a way of meeting people. They also share their love for their natural surroundings and discuss the challenges this rural area is currently facing, particularly in relation to housing and community coherence.'
---
