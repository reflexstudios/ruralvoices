---
id: deb26788-efc8-4580-a26b-c426b7384325
blueprint: stories
title: 'Vida Lake'
age: '75'
town: Omagh
county: tyrone
images:
  - Vida-with-grandchildren.jpg
  - Vida-with-sister.JPG
  - Vida.JPG
audio_highlights:
  -
    audio_highlight_title: 'Moving to Northern Ireland'
    audio_highlight_text: 'Vida talks about growing up in Asia, the persecution of Baha''i people and moving to Northern Ireland.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-to-NI-.mp3
  -
    audio_highlight_title: 'Experience of the Troubles'
    audio_highlight_text: 'Vida shares her first impressions of Northern Ireland and moving here at the height of the Troubles.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-First-impressions-of-NI.mp3
  -
    audio_highlight_title: 'The Omagh Bombing'
    audio_highlight_text: 'Vida recalls her work with the St John’s Ambulance service during the Omagh Bombing in 1998.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Omagh-Bomb.mp3
  -
    audio_highlight_title: 'Past and future changes'
    audio_highlight_text: 'Vida talks about how Omagh has changed and shares her hopes for a more integrated and united future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Changes-and-hopes-for-the-future.mp3
person_profile_image: Vida.JPG
story_tags:
  - religion
  - bame
  - migration
  - troubles
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646822587
main_audio: Vida-Lake-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Vida was born in Iran, grew up in Indonesia and moved to England and then to Northern Ireland. She first lived in Derry/Londonderry and then moved to Omagh due to her husband’s work. She talks about her Baha''i faith and the persecution her family suffered in Iran. She shares her first impressions of Northern Ireland, moving here at the height of the Troubles and recalls her work with the St John’s Ambulance service during the Omagh Bombing in 1998. She also talks about shared education, migrant communities in Omagh and shares her hopes for a more integrated future.'
---
