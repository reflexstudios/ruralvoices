---
id: 8b7cf3c4-f25b-46d3-930b-d03617770316
blueprint: stories
title: 'Raymond Green'
age: '71'
town: Templepatrick
county: antrim
images:
  - RGHGreen.jpg
  - Snow-in-Templepatrick.jpg
  - View-in-Templepatrick.jpg
audio_highlights:
  -
    audio_highlight_title: 'Moving to Templepatrick'
    audio_highlight_text: 'Raymond talks about moving to Templepatrick and how medical services have changed over the years.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1-Moving-to-Templepatrick-and-GP-services.mp3
  -
    audio_highlight_title: 'Living in Templepatrick'
    audio_highlight_text: 'Raymond describes Templepatrick, changes over the years and the pros and cons of living there.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2-Pros-and-Cons.mp3
  -
    audio_highlight_title: 'Hearing loss and accessibility'
    audio_highlight_text: 'Raymond talks about his loss of hearing and lack of accessibility in venues in rural and urban areas.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3-Deaf-challenges.mp3
  -
    audio_highlight_title: 'Christian faith'
    audio_highlight_text: 'Raymond talks about his Christian faith and the impact of his beliefs on his medical practice.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4-Being-a-Christian-doctor.mp3
person_profile_image: RGHGreen.jpg
story_tags:
  - disability
  - religion
  - templepatrick
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646819572
main_audio: Raymond-Green-full-interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Raymond is from North Belfast, but moved to Templepatrick, Co. Antrim, 35 years ago to work as a GP. He talks about how medical services have changed in the area and compares it to other places. Raymond describes Templepatrick and how it went from a quiet village to an urban place since the opening of the road to the airport. He also mentions how it was affected by the Troubles, the pros and cons of living there and the changes the area is likely to see in the future. He talks about losing his hearing and the lack of accessible services in both rural and urban areas. He also discusses his Christian faith, its impact on his medical practice, and shares his hope for a more tolerant future.'
---
