---
id: d552f678-76d8-4347-bb9f-fc2269c237ce
blueprint: stories
title: 'Ruth Montgomery'
age: '35'
town: 'Castlederg & Omagh'
county: tyrone
images:
  - Ruth-and-snow3.jpg
  - Ruth-and-snow.jpg
  - Ruth-and-snow2.jpg
audio_highlights:
  -
    audio_highlight_title: 'The Dergfest'
    audio_highlight_text: 'Ruth describes Castlederg and how it has changed from a sectarian place to a lively town home to the famous Dergfest.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Describing-Castlederg.mp3
  -
    audio_highlight_title: 'Impact of the Troubles'
    audio_highlight_text: 'She talks about the impact of the Troubles in Castlederg and how good relations have improved over the years.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Castlederg-and-Troubles.mp3
  -
    audio_highlight_title: 'Living in county Tyrone'
    audio_highlight_text: 'Ruth reflects on the strengths and limitations of living in places such as Omagh and Castlederg.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Pros-and-Cons-of-rural-area.mp3
  -
    audio_highlight_title: 'Good relations work'
    audio_highlight_text: 'Ruth talks about her good relations work and the differences between social housing in rural and urban areas.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Social-Housing-and-good-relations.mp3
  -
    audio_highlight_title: 'Hopes for Castlederg and Omagh'
    audio_highlight_text: 'Ruth shares her hopes for Castlederg and Omagh’s future, including more job opportunities for young people.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Hopes-for-the-future.mp3
person_profile_image: Ruth-and-snow3.jpg
story_tags:
  - housing
  - community-relations
  - castlederg
  - festivals
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821009
main_audio: Ruth-Montgomery-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: "Ruth grew up in Castlederg, Co. Tyrone, and currently lives in Omagh after spending 14 years in Belfast. She shares memories of her rural childhood and describes how Castlederg has changed over the years, from being heavily impacted by sectarianism to becoming a lively town, home to one of Northern Ireland’s main community festivals, The Dergfest. She describes Castlederg’s strong sense of community, farming activity, nature and reflects on the strengths and limitations of county Tyrone.\_ She also talks about her good relations work and discusses the differences between social housing issues in rural and urban areas."
---
