---
id: 83e43dd9-40a6-4c4c-ab37-6f2574324f6c
blueprint: stories
title: 'Jodi Morrison'
age: '17'
town: Glenavy
county: antrim
images:
  - people/Horseridding.jpg
  - people/Horseridding2.jpeg
  - people/Jodie-as-a-Child.jpg
audio_highlights:
  -
    audio_highlight_title: 'The fugitive goat'
    audio_highlight_text: 'Jodi shares her love for animals and tells a funny story about her aunt’s fugitive goat.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Goat-Story.mp3
  -
    audio_highlight_title: 'Rural living for young people'
    audio_highlight_text: 'Jodi discusses the pros and cons of rural living for young people.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Pros-and-Cons.mp3
  -
    audio_highlight_title: 'Plans for the future'
    audio_highlight_text: 'Jodi talks about her plans for the future and how she would like to continue living in a rural area.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Her-plans-for-the-future.mp3
  -
    audio_highlight_title: 'Hopes for Glenavy'
    audio_highlight_text: 'Jodi shares her hopes for Glenavy to continue being rural, safe and tolerant.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Her-hopes-for-the-future.mp3
person_profile_image: people/Horseridding-1646406058.jpg
story_tags:
  - youth
  - farming
  - glenavy
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646819301
main_audio: Jodi-Morrison-Full-interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Jodi has always lived in Glenavy, Co. Antrim and despite wanting to study Filmmaking or Veterinary she would like to stay there. She shares her love for country life, its easy access to nature, the abundance of animals, and safety. She describes the close-knit and tolerant aspects of Glenavy and some changes the area has experienced over the years. She also talks about the lack of public transport services and the pressures of urbanisation and how she would not like to see the fields around her turning into housing estates.'
---
