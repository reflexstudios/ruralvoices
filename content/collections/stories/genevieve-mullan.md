---
id: 751825e3-bd60-4684-90ca-b22463441270
blueprint: stories
title: 'Genevieve Mullan'
age: '57'
town: Ballerin
county: derry
images:
  - Beekeeping.jpg
  - Genevieve-and-Jude.jpeg
  - Genevieve.jpg
audio_highlights:
  -
    audio_highlight_title: 'Spelling controversy'
    audio_highlight_text: 'Genevieve describes her local area and the controversy behind Ballerin’s spelling.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Name-controversy.mp3
  -
    audio_highlight_title: 'Husband''s ingenuity'
    audio_highlight_text: 'Genevieve shares the story about her husband building a footpath for their children to safely wait for the school bus.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Lack-of-footpath.mp3
  -
    audio_highlight_title: 'Buying a farm'
    audio_highlight_text: 'Genevieve talks about buying a beef farm and her husband''s improvised Christmas presents.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Xmas-presents.mp3
  -
    audio_highlight_title: 'Cattle poison'
    audio_highlight_text: 'Genevieve recalls the day their cattle were poisoned and they lost 30 years of work.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Lead-poisoning-1646822492.mp3
  -
    audio_highlight_title: 'An unconventional farmer’s wife'
    audio_highlight_text: 'Genevieve talks about her unconventional educational path for a farmer’s wife, going from a dyslexic teen to nursing.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Not-a-typical-farmers-wife-1646822521.mp3
  -
    audio_highlight_title: 'Farming changes'
    audio_highlight_text: 'Genevieve describes how farming has changed from a self-sustaining activity to being highly dependent on heavy machinery.'
    type: new_audio
    enabled: true
    audio_highlight_file: 6.-How-farming-has-changed.mp3
person_profile_image: Genevieve.jpg
story_tags:
  - farming
  - women
  - ballerin
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646822534
main_audio: Genevieve-Mullan-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Genevieve talks about her farming life and how it has gone from a primitive self-sustaining activity to highly dependent on heavy machinery and big fields. She shares stories of family life in the country, particularly how her husband’s ingenuity has been put to good use to make things such as a footpath to wait for the school bus safely. She also talks about not being a typical farmer’s wife and recalls the day their cattle were poisoned and they lost 30 years of work. She shares her hopes for a more self-sustainable and environmentally-friendly way of farming.'
---
