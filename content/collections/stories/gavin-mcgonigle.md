---
id: a6aa236e-4145-4b9f-8318-aa7abadefbe8
blueprint: stories
title: 'Gavin McGonigle'
age: '44'
town: Macosquin
county: derry
images:
  - MACOSQUIN-BRIDGE.jpg
  - photo-Gavin.jpeg
  - Play-Park-2.jpg
audio_highlights:
  -
    audio_highlight_title: 'Describing Macosquin'
    audio_highlight_text: 'Gavin describes Macosquin and talks about the work of his voluntary committee.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Local-area-and-committee-work.mp3
  -
    audio_highlight_title: 'Local tales'
    audio_highlight_text: 'Gavin shares Macosquin’s mythical history, heritage and some local stories.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-history-and-tales.mp3
  -
    audio_highlight_title: 'Community spirit'
    audio_highlight_text: 'Gavin discusses the strong community spirit and how this was important during the Covid pandemic.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-favourite-community-spirit.mp3
  -
    audio_highlight_title: 'The importance of voluntary work'
    audio_highlight_text: 'Gavin talks about the committee’s future plans for Macosquin and the importance of voluntary groups.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Future-projects-importance-of-committee.mp3
person_profile_image: photo-Gavin.jpeg
story_tags:
  - macosquin
  - community-relations
  - heritage
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821810
main_audio: Gavin-Full-Interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Gavin has been living in Macosquin for the past 20 years and talks about his volunteer work with the local community. He describes the quiet rural nature of Macosquin and shares its mythical history and heritage. Gavin highlights the strong sense of community in his area and people’s pride of the place and talks about the challenges of living in a rural village. He discusses how much the area has changed and how voluntary groups such as his have brought many benefits to the local community.'
---
