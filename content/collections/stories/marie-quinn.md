---
id: 5675ba6e-c45b-454a-aab4-411ecd279b21
blueprint: stories
title: 'Marie Quinn'
age: '76'
town: Aghaginduff
county: tyrone
images:
  - Marie-gardening.jpeg
  - Marie-bluebells.jpg
  - Marie-origami.jpg
audio_highlights:
  -
    audio_highlight_title: 'Childhood and nursing'
    audio_highlight_text: 'Marie talks about growing up on a farm, studying nursing and winning an unusual award for Catholics.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Growing-Up-in-the-Glen-and-Education.mp3
  -
    audio_highlight_title: 'Raising a family'
    audio_highlight_text: 'Marie talks about raising a family of 7 children and losing one of her sons to cancer.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Raising-a-family-of-7-children.mp3
  -
    audio_highlight_title: 'The glens'
    audio_highlight_text: 'Marie shares her love for her rural place through a poem about the glens.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Love-for-her-place-and-writing.mp3
  -
    audio_highlight_title: 'Paddy''s cottage poem'
    audio_highlight_text: 'Maria reads her poem about her memories of Paddy''s cottage.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Paddys-Cottage-Poem-BEST.mp3
  -
    audio_highlight_title: 'A poem about my father'
    audio_highlight_text: 'Maria shares a poem about what life was like for her and her father.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Poem-about-she-and-her-father.mp3
  -
    audio_highlight_title: 'Rural living changes'
    audio_highlight_text: 'Maria discusses how much rural life has changed over the last 100 years.'
    type: new_audio
    enabled: true
    audio_highlight_file: 6.-How-much-it-has-changed.mp3
person_profile_image: Marie-gardening.jpeg
story_tags:
  - farming
  - education
  - aghaginduff
  - women
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646819652
main_audio: Marie-Quinn-Full-Interview.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Marie was born in a dairy and herd farm in the same land as her ancestors. As the only child of a farmer, she talks about how her father wanted her to take over the farm, but she preferred to study Nursing. She shares her educational journey and talks about the difficulties of raising a big family. She mentions her love for writing and local history and shares some poetry she wrote about her fond memories of Paddy’s Cottage and of her father. She reflects on how much her rural area has changed over the years, how farming and income generation dynamics are different now and shares her hopes for the future.'
---
