---
id: a812d40f-a87c-47b3-b53e-331933b7197d
blueprint: stories
title: 'Simon Wiggins'
age: '31'
town: Newtownbutler
county: fermanagh
images:
  - Marching.jpg
  - Simon-golf.jpg
  - Simon-speech.jpg
audio_highlights:
  -
    audio_highlight_title: 'Local history'
    audio_highlight_text: 'Simon talks about Newtonbutler''s rich history, the impact of the border and shares some smuggling tales.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Rich-history-and-border.mp3
  -
    audio_highlight_title: 'Marching tradition'
    audio_highlight_text: 'Simon shares his views on the marching tradition and its importance for young people.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Marching-and-Parades.mp3
  -
    audio_highlight_title: 'Portraying the marching tradition'
    audio_highlight_text: 'Simon discusses the limited portrayals of the marching tradition and how it has changed over the years.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Misrepresentation.mp3
  -
    audio_highlight_title: 'Growing up in Fermanagh'
    audio_highlight_text: 'Simon recalls his experiences of intimidation while growing up in a Republican town.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Memories-of-intimidation.mp3
  -
    audio_highlight_title: 'United Ireland'
    audio_highlight_text: 'Simon shares his views on a United Ireland and his hopes for the future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 5.-Future-hopes-and-UI.mp3
person_profile_image: Simon-speech.jpg
story_tags:
  - marching
  - border
  - united-ireland
  - community-relations
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646823054
main_audio: Simon-Wiggins-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Simon talks about Newtonbutler''s rich history, beautiful surroundings, and how it has been affected by the border. He recalls some local tales about smuggling and discusses community relations in the area. He talks about the marching tradition, his involvement in it, its contested nature and its importance for young people in rural communities. He also shares childhood memories of intimidation for belonging to a minority community and talks about his work with cultural exchange in County Fermanagh. He shares his views on a united Ireland and his hopes for a future with stronger local businesses and better community relations.'
---
