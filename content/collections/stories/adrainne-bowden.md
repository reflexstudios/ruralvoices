---
id: bb4ee9f1-71a9-410f-b700-039ca2071ea6
blueprint: stories
title: 'Adrainne Bowden'
age: '83'
town: Kilbride
county: down
person_profile_image: 'people/Adrainne Bowden.jpeg'
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646818755
images:
  - 'people/Adrainne Bowden.jpeg'
  - 'people/Taj Mahal - please credit Follies Trust .jpeg'
main_audio: Adrainne-Full-Interview.mp3
audio_highlights:
  -
    audio_highlight_title: 'Kilbridge and the Taj Mahal'
    audio_highlight_text: 'Adrainne describes Kilbride and one of its most distinctive buildings: a replica of the Taj Mahal.'
    audio_highlight_file: 1-Killbride-and-the-Taj-Mahal.mp3
    type: new_audio
    enabled: true
  -
    audio_highlight_title: 'Growing up in Ballyrobert'
    audio_highlight_text: 'Adrainne recalls her childhood in Ballyclare, the car races and the presence of soldiers during the Second World War.'
    audio_highlight_file: 2-Growing-up-in-Ballyrobert.mp3
    type: new_audio
    enabled: true
  -
    audio_highlight_title: 'Childhood Now and Then'
    audio_highlight_text: 'Adrainne compares the difference of growing up in a rural area across generations.'
    audio_highlight_file: 3-Childhood-Now-and-Then.mp3
    type: new_audio
    enabled: true
  -
    audio_highlight_title: 'Lockdown and Church-going'
    audio_highlight_text: 'Adrainne talks about church going during the lockdown and how it has changed over the years.'
    audio_highlight_file: 4-Lockown-and-Church-going.mp3
    type: new_audio
    enabled: true
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
story_tags:
  - religion
  - kilbride
  - childhood
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Adrainne is originally from Lisnalinchy, Co. Antrim, but moved to Kilbride, Co. Down, after getting married. She talks about one of the area’s most distinctive buildings - a replica of the Taj Mahal - and how the area has changed over the years. She also recalls her childhood in Ballyclare and reflects on her family’s childhood across generations. She shares her views on church-going and hopes for the future.'
---
