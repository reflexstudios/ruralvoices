---
id: 1b1a056e-28f1-4ce5-abe7-b2329ca0dbb4
blueprint: stories
title: 'Ana Stewart'
age: '63'
town: Magherafelt
county: derry
images:
  - Ana-Morery-sola.jpg
  - Ana-with-family.png
audio_highlights:
  -
    audio_highlight_title: 'From Colombia to Magherafelt'
    audio_highlight_text: 'Ana talks about moving to Magherafelt and adapting to the weather, car dependency and homesickness.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Moving-to-NI-and-adaptation.mp3
  -
    audio_highlight_title: 'Raising a family abroad'
    audio_highlight_text: 'Ana recalls the challenges of raising a family in a rural area and her experiences of education.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Motherhood-and-schooling.mp3
  -
    audio_highlight_title: 'Describing Magherafelt'
    audio_highlight_text: 'Ana talks about how Magherafelt has changed over the years and what she likes and dislikes about it.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-changes.mp3
  -
    audio_highlight_title: 'Hopes for the future'
    audio_highlight_text: 'Ana shares hopes for better technology, more environmental awareness and English courses for foreigners in rural areas.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Hopes-for-the-future-1646822632.mp3
person_profile_image: Ana-Morery-sola.jpg
story_tags:
  - migration
  - bame
  - disability
  - education
  - magherafelt
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646822640
main_audio: Ana-Stewart-Full.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Ana moved from Bogota, Colombia, to Magherafelt, Co. Derry/Londonderry in 1994 to be with her husband. She talks about moving from a big city to a rural Northern Irish town and compares city living to rural living. She likes the quietness of country living but recalls having to adapt to the weather, car dependency and distance from homeland. She also talks about the challenges of motherhood as a migrant as well as a mother of a child with Asperger’s syndrome. She recalls how Magherafelt has changed and shares her hopes for a more environmentally friendly future.'
---
