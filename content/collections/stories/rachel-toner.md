---
id: 68859e08-0c6a-427b-910a-8b8553068b84
blueprint: stories
title: 'Rachel Toner'
age: '27'
town: Armagh
county: armagh
images:
  - Child.png
  - Rachel-toner.jpeg
  - Armagh.jpg
audio_highlights:
  -
    audio_highlight_title: 'Resilient Armagh'
    audio_highlight_text: 'Rachel describes Armagh’s resilient nature and her favourite things about it.'
    type: new_audio
    enabled: true
    audio_highlight_file: 1.-Armaghs-resilience-and-positives-1646821132.mp3
  -
    audio_highlight_title: 'Living in a rural city'
    audio_highlight_text: 'Rachel talks about a number of issues her rural city encounters.'
    type: new_audio
    enabled: true
    audio_highlight_file: 2.-Challenges.mp3
  -
    audio_highlight_title: 'Moving on from the Troubles'
    audio_highlight_text: 'She describes how Armagh has wisely used its rich cultural heritage and history to move on from the Troubles.'
    type: new_audio
    enabled: true
    audio_highlight_file: 3.-Past-Changes.mp3
  -
    audio_highlight_title: 'Arts and culture'
    audio_highlight_text: 'She highlights current cultural activities and shares her hopes for the future.'
    type: new_audio
    enabled: true
    audio_highlight_file: 4.-Future-changes.mp3
person_profile_image: Rachel-toner.jpeg
story_tags:
  - armagh
  - community-relations
  - heritage
use_meta_keywords: false
no_index_page: false
no_follow_links: false
sitemap_priority: '0.5'
sitemap_changefreq: daily
override_twitter_settings: false
override_twitter_card_settings: false
twitter_card_type_page: summary
updated_by: 063e0dda-308a-428f-8672-ec1683f46b63
updated_at: 1646821167
main_audio: Rachel-Toner-Full-1646821138.mp3
content:
  -
    type: paragraph
    content:
      -
        type: text
        text: "Rachel shares her love for her hometown and praises this rural city’s resilience after being badly affected by the Troubles. She highlights some of the main sights and how its rich cultural heritage and history are being used to bring in new opportunities for locals. Rachel talks about a number of issues her rural city encounters, including the inaccessibility of public transport, unreliable internet connection and lack of bustling nightlife. She discusses the work that younger generations are doing around climate change and community relations and believes that Armagh has a bright future ahead.\_"
---
