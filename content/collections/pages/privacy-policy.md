---
id: a5e1baa5-118a-465a-9860-1bc8fbdf8d2d
blueprint: about
template: about
title: 'Privacy Policy'
author: f953e5bb-700a-4f9c-b189-858f84444952
updated_by: f953e5bb-700a-4f9c-b189-858f84444952
updated_at: 1646042725
content:
  -
    type: heading
    attrs:
      level: 1
    content:
      -
        type: text
        text: 'Privacy Policy'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Privacy Policy goes here.'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!'
      -
        type: hard_break
      -
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!'
      -
        type: hard_break
      -
        type: text
        text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum fugit quia iure nisi laborum maxime quisquam error harum in fuga ab earum consequatur sequi, non molestiae tenetur vel facilis expedita!'
---
