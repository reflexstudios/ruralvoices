---
id: 2509fbe8-8979-47ae-ad01-5406e2d71775
blueprint: take_part
title: 'Take Part'
template: takePart
author: f953e5bb-700a-4f9c-b189-858f84444952
updated_by: f953e5bb-700a-4f9c-b189-858f84444952
updated_at: 1646042559
content:
  -
    type: heading
    attrs:
      level: 1
    content:
      -
        type: text
        text: 'Would you like to add your story?'
      -
        type: hard_break
  -
    type: paragraph
    content:
      -
        type: hard_break
      -
        type: text
        text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
  -
    type: paragraph
    content:
      -
        type: text
        text: ' Quidem, ullam non! Natus ea earum placeat perferendis fugit consequuntur cum officiis, temporibus rem tempora'
  -
    type: paragraph
    content:
      -
        type: text
        text: ' omnis reiciendis, perspiciatis modi, ex repellat quas?'
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: instructions
        instruction:
          -
            text:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Explore some of the '
                  -
                    type: text
                    marks:
                      -
                        type: link
                        attrs:
                          href: 'statamic://entry::210c05ae-1b98-436d-906a-8f5a4a7a0ffb'
                          rel: null
                          target: null
                          title: null
                    text: 'existing stories'
                  -
                    type: text
                    text: '. Think about how you would tell your story in a similar'
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: ' format and think about the key events or highlights of the story.'
          -
            text:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Use the form below to tell us your story, if you can upload images and voice recordings to help capture key events of your story we will be more likely to use your story.'
          -
            text:
              -
                type: paragraph
                content:
                  -
                    type: text
                    text: 'Once you have submitted your story someone from our team will be in touch with you if we would like to include your story live on the project.'
  -
    type: paragraph
---
