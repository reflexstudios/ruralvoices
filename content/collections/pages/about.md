---
id: 7d3fa5a1-09df-480b-83e2-6f92fa5cb198
blueprint: about
title: About
template: about
author: f953e5bb-700a-4f9c-b189-858f84444952
updated_by: f953e5bb-700a-4f9c-b189-858f84444952
updated_at: 1646041001
content:
  -
    type: heading
    attrs:
      level: 1
    content:
      -
        type: text
        text: 'About the Project'
  -
    type: heading
    attrs:
      level: 6
    content:
      -
        type: text
        text: 'The Rural Voices project, led by the Rural Community Network and supported by the Shared History Fund managed by the National Lottery Heritage Fund, is a collection of contemporary stories from rural communities and people across Northern Ireland.'
  -
    type: paragraph
    content:
      -
        type: hard_break
      -
        type: hard_break
  -
    type: paragraph
    content:
      -
        type: text
        text: 'This website features an archive of stories and histories from people across the country reflecting on their lives, their locality and what makes their area unique.'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'Interviews offer a broad and varied representation of what it means to live in Northern Ireland today - 100 years on from events that created the region.'
      -
        type: hard_break
      -
        type: hard_break
      -
        type: text
        text: 'Rural Voices aims to show the diversity of life in Northern Ireland today and how it has changed over time.'
  -
    type: paragraph
    content:
      -
        type: hard_break
      -
        type: hard_break
  -
    type: set
    attrs:
      values:
        type: funders
        funders:
          -
            funder_logo: placeholders/company1.png
          -
            funder_logo: placeholders/RCN.png
          -
            funder_logo: placeholders/heratigeFund.png
          -
            funder_logo: placeholders/NIOffice.png
  -
    type: paragraph
---
