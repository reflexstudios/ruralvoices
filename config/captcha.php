<?php

return [
    'service' => 'Recaptcha', // options: Recaptcha / Hcaptcha
    'sitekey' => env('CAPTCHA_SITEKEY', '6LcjQXkeAAAAALj08aNzMnyIj0nQpPqPOSrAot6G'),
    'secret' => env('CAPTCHA_SECRET', '6LcjQXkeAAAAAJgscm1LiNAkpHbUzKWbpYh8Uxdv'),
    'collections' => [],
    'forms' => ['take_part'],
    'user_login' => false,
    'user_registration' => false,
    'disclaimer' => '',
    'invisible' => false,
    'hide_badge' => false,
    'enable_api_routes' => false,
];
