module.exports = {
  mode: 'jit',
  purge: [ 
  './resources/**/*.antlers.html',
  './resources/**/*.js',
  './resources/**/*.vue',
],

  darkMode: false, // or 'media' or 'class'

  
  theme: {
    extend: {

      transitionDelay: {
        '0': '0ms',
        '1700': '1700ms',
        '2000': '2000ms',
        '3000': '3000ms',
      },

      screens: {
        'break1': '1312px',
      },     

      fontSize: {
          'largestTitle': '6.5rem',            // 104px
          '6xl': '4.063rem',            // 65px
          '5xl': '3.375rem',            // 54px
          'fifty': '3.125rem',            // 50px
          '4xl': '2.875rem',            // 46px
          '3xl': '2.125rem',            // 34px
          'thirty': '1.875rem',            // 30px
          'twentyEight': '1.65rem',            // 30px
          '2.6xl': '1.625rem',            // 26px
          '2.4xl': '1.5rem',            // 24px
          '2xl': '1.25rem',            // 20px
          '1xl': '1.125rem',            // 18px
          'normal': '1rem',            // 16px
          '1xs': '0.875rem',            // 14px
          '2xs': '0.75rem',            // 12px
          'ten': '0.625rem',            // 10px
          '3xs': '0.688rem',            // 11px

      },        
      
    
      lineHeight: {   
        'oneofour': '6.5rem',               // 104px
        'nineyone': '5.713rem',               // 91.4px
        'fiftynine': '3.713rem',               // 59.4px
        'fortyeight': '3rem',               // 48px
        'thirtyseven': '2.338rem',               // 37.4px
        'thirtytwo': '2rem',               // 32px
      },


      colors: {
        darkBlue:'#404085',            // blue
        lighterBlue:'#7A78B4',            // blue
        seaGreen: '#488A8B',           //sea green
        mustard: '#FED390',           //mustard
        redy: '#DB5858',           //redy
        darkestBlue:'#1D1D3C',            // darkestblue
        nearBlackBlue: '#101025',         // nearBlackBlue
        cream:'#FEF9E8',            // cream
        pinky:'#FDB7A5',            // pinky
        darkerCream: '#F2E2D0' ,            //darkerCream

        
     
        spacing: {
          '1.25': '0.3125rem',      //5px            
        },
     
        maxWidth: {
            
         }
      },

      zIndex: {
        '100': '100',
        '101': '101',
        '102': '102',
      },

      rotate: {
        '360': '360deg',
        'minus20': '-20deg',
        '20': '20deg',
      },


      fontFamily: {             
        'manrope': ['Manrope, sans-serif'],
      },    

      transitionDuration: {
        '0': '0ms',
        '2000': '2000ms',
        '3000': '3000ms',
        '5000': '5000ms',
        '8000': '8000ms',
      },

  


  },
  variants: {
    extend: {
      scale: ['active', 'group-hover'],
      textColor: ['group-hover'],
      borderRadius: ['hover', 'focus'],
      mixBlendMode: ['hover', 'focus'],
      translate: ['hover', 'group-hover'],
      opacity: ['hover', 'group-hover'],
      transform: ['hover', 'group-hover'],
      
    },
  },
  plugins: [],
}
}
