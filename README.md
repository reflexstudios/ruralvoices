# Homeless Prevention Forum
DEV: 

username: hello@reflex-studios.com
password: reflexstudios

## Setup
- git clone git@bitbucket.org:reflexstudios/ruralvoices.git
- cd /sitename
- composer install
- cp .env.example .env && php artisan key:generate
- If using Tailwind JIT mode make sure package.json has - "watch": "TAILWIND_MODE=watch mix watch",

## Valet:
- valet link
- valet park
- valet open
## Dev:
- npm install
- npm run dev
- npm run watch - If using Tailwind JIT mode make sure package.json has - "watch": "TAILWIND_MODE=watch mix watch",

## Dev Site 
ruralvoices.reflex-dev.com
username: ruralvoicesrefle
password: 3A1slU#8.F*X
ssh -p 4835 ruralvoicesrefle@185.211.22.175




